/*
 ===============================================================================
 Name        : main.c
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main definition
 ===============================================================================
 */

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>
#include "FreeRTOS.h"
#include "task.h"

// TODO: insert other include files here
#include "DigitalIoPin.h"
#include "ITM_write.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// TODO: insert other definitions and declarations here

#include "semphr.h"
#include <mutex>
#include "Fmutex.h"
#include "user_vcom.h"

struct Cmd {
	uint8_t r;
	uint8_t g;
	uint8_t b;
};

static void prvSetupHardware();
void vConfigureTimerForRunTimeStats();
void SCT_Init();
void set_motor_angle(int angle);
static void task1(void *pvParameters);
static void receive_task(void *pvParameters);

QueueHandle_t cmd_queue;

int main(void) {
	prvSetupHardware();
	ITM_init();
	cmd_queue = xQueueCreate(2, sizeof(Cmd));

	SCT_Init();
	Chip_SWM_Init();
	Chip_SWM_MovablePortPinAssign(SWM_SCT0_OUT0_O, 0, 3); // LED Green
	Chip_SWM_MovablePortPinAssign(SWM_SCT0_OUT1_O, 1, 1); // LED Blue
	Chip_SWM_MovablePortPinAssign(SWM_SCT1_OUT0_O, 0, 25); // LED Red

	xTaskCreate(task1, "led task", configMINIMAL_STACK_SIZE + 256, NULL,
			(tskIDLE_PRIORITY + 1UL), (TaskHandle_t *) NULL);

	xTaskCreate(receive_task, "Rx", 256, NULL, (tskIDLE_PRIORITY + 1UL),
				(TaskHandle_t *) NULL);

	xTaskCreate(cdc_task, "CDC", 256, NULL, (tskIDLE_PRIORITY + 1UL),
			(TaskHandle_t *) NULL);

	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;
}

extern "C" {

void vConfigureTimerForRunTimeStats() {
	Chip_SCT_Init(LPC_SCTSMALL1);
	LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
	LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}
}

static void prvSetupHardware() {
	SystemCoreClockUpdate();
	Board_Init();

	Board_LED_Set(0, false);
	Board_LED_Set(1, false);
	Board_LED_Set(2, false);
 }

void SCT_Init(void)
{
	Chip_SCT_Init(LPC_SCT0);
	Chip_SCT_Init(LPC_SCT1);

    LPC_SCT0->CONFIG |= (1 << 17) | (1 << 18); // two 16-bit timers, auto limit L, H
    LPC_SCT0->CTRL_L |= SCT_CTRL_PRE_L(72-1); // set prescaler, SCTimer/PWM clock = 1 MHz
    LPC_SCT0->CTRL_H |= SCT_CTRL_PRE_H(72-1); // set prescaler, SCTimer/PWM clock = 1 MHz

    //match & reload
    LPC_SCT0->MATCHREL[0].L = 1000-1; // match 0 @ 1 kHz PWM freq
    LPC_SCT0->MATCHREL[1].L = 1000; // match 1 used for duty cycle,

    LPC_SCT0->MATCHREL[0].H = 1000-1; // match 0 @ 1 kHz PWM freq
    LPC_SCT0->MATCHREL[1].H = 1000; // match 1 used for duty cycle,

    //Page 255
    //4 HEVENT Select L/H counter.(0 = low, 1 = high)
    //13:12 COMBMODE Selects how the specified match and I/O condition are used and combined. (0x1 = MATCH)
    //

    LPC_SCT0->EVENT[0].STATE = 0xFFFFFFFF; // event 0 happens in all states
    LPC_SCT0->EVENT[0].CTRL = (0 << 4) | (1 << 12); // L state, match 0 condition only

    LPC_SCT0->EVENT[1].STATE = 0xFFFFFFFF; // event 1 happens in all states
    LPC_SCT0->EVENT[1].CTRL = (0 << 4) | (1 << 0) | (1 << 12); // L state, match 1 condition only

    LPC_SCT0->EVENT[2].STATE = 0xFFFFFFFF; // event 2 happens in all states
    LPC_SCT0->EVENT[2].CTRL = (1 << 4) | (1 << 12); // H state, match 0 condition only

    LPC_SCT0->EVENT[3].STATE = 0xFFFFFFFF; // event 3 happens in all states
    LPC_SCT0->EVENT[3].CTRL = (1 << 4) | (1 << 0) | (1 << 12); // H state, match 1 condition only

    /* event 0 will set SCT0_OUTx, event 1 will clear SCT0_OUTx */
    LPC_SCT0->OUT[0].SET = (1 << 0); // event 0 will set SCT0_OUTx
    LPC_SCT0->OUT[0].CLR = (1 << 1); // event 1 will clear SCT0_OUTx

    LPC_SCT0->OUT[1].SET = (1 << 2); // event 2 will set SCT0_OUTx
    LPC_SCT0->OUT[1].CLR = (1 << 3); // event 3 will clear SCT0_OUTx

    //Timer 1 L
	LPC_SCT1->CONFIG |= (1 << 17); // two 16-bit timers, auto limit
	LPC_SCT1->CTRL_L |= SCT_CTRL_PRE_L(72-1); // set prescaler, SCTimer/PWM clock = 1 MHz

	LPC_SCT1->MATCHREL[0].L = 1000 - 1; // match 0 @ 1 kHz PWM freq
	LPC_SCT1->MATCHREL[1].L = 1000; // match 1 used for duty cycle, = 5%

	LPC_SCT1->EVENT[0].STATE = 0xFFFFFFFF; // event 0 happens in all states
	LPC_SCT1->EVENT[0].CTRL = (1 << 12); // match 0 condition only

	LPC_SCT1->EVENT[1].STATE = 0xFFFFFFFF; // event 1 happens in all states
	LPC_SCT1->EVENT[1].CTRL = (1 << 0) | (1 << 12); // match 1 condition only

	LPC_SCT1->OUT[0].SET = (1 << 0); // event 0 will set SCTx_OUT0
	LPC_SCT1->OUT[0].CLR = (1 << 1); // event 1 will clear SCTx_OUT0

	LPC_SCT1->CTRL_L &= ~(1 << 2); // unhalt all timer by clearing bit 2 of CTRL reg, from example
	LPC_SCT0->CTRL_L &= ~(1 << 2);
	LPC_SCT0->CTRL_H &= ~(1 << 2);
}

static void task1(void *pvParameters) {
	Cmd commandObj;
	while (1) {
		xQueueReceive(cmd_queue, &commandObj, portMAX_DELAY);
		// leds work with inverted logic
        LPC_SCT1->MATCHREL[1].L = 1000 - commandObj.r*1000/255;
        LPC_SCT0->MATCHREL[1].L = 1000 - commandObj.g*1000/255;
        LPC_SCT0->MATCHREL[1].H = 1000 - commandObj.b*1000/255;
	}
}

static void receive_task(void *pvParameters) {
	Cmd commandObj;
	char str[65];
	memset(str, '\0', 65);
	char pattern[] = "rgb #";
	char *position;
	while (1) {
		USB_receive((uint8_t *) str, RCV_BUFSIZE);
		USB_send((uint8_t*) str, strlen(str));

		if (position = strstr(str, pattern)) {
			int raw_value = (int)strtol(position + strlen(pattern), NULL, 16);
			commandObj.r = raw_value / 0x10000;
			commandObj.g = (raw_value / 0x100) % 0x100;
			commandObj.b = raw_value % 0x100;
			snprintf (str, 64, "raw value = 0x%x, r = 0x%x, g = 0x%x, b = 0x%x \r\n", raw_value, commandObj.r,commandObj.g, commandObj.b);
			USB_send((uint8_t*) str, strlen(str));
			xQueueSendToBack(cmd_queue, &commandObj, portMAX_DELAY);
		}
		memset(str, '\0', 65);
	}
}

