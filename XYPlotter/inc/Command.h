/*
 * Command.h
 *
 *  Created on: 29.09.2017
 *      Author: auril
 */

#ifndef COMMAND_H_
#define COMMAND_H_

class Command {
private:
	// command according to command-table (in OneNote)
	int cmd;		// code for command

	/* NOTE: coords are saved in mm *100 since mDraw sends values as mm with 2 digits after point precision
	 * so unit is cmm "centimillimeter"
	 */
	int x;			// x coordinate
	int y;			// y coordinate

	// delay for command execution?? or: abs/rel coordinates -- always A0?
	int a;		// value after the A in G1 commands)

	// this is to control servo and laser
	int value;		// value in M1 or M4 commands for Servo or Laser
public:

	int getCmd();
	int getX();
	int getY();
	int getA();
	int getValue();

	Command();						// constructor for empty commands -- there are no setters, so don't use it unless you need an invalid command
	Command(int cmd, int X, int Y, int A);	// constructor for commands with coordinates
	Command(int cmd, int value);	// constructor for commands for pen or laser
	virtual ~Command();
};

#endif /* COMMAND_H_ */
