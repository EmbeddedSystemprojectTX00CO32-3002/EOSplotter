/*
 * XYPlotter.h
 *
 *  Created on: 28.09.2017
 *      Author: auril
 */

#ifndef XYPLOTTER_H_
#define XYPLOTTER_H_


#include "FreeRTOS.h"
#include "queue.h"				// for queues 			// freeRTOS


#define QUCMDPARSERLENGTH 50	// max items on command queue of type Command (Class)
#define CMDSEGMENTSMAX 6		// count of arguments in the arguments array in class Command - TODO should be reduced to the actual max count of arguments of the used commands


//global settings
extern int gsXAxisDistanceUM;		// size of Y axis (using µm for better usage of commands --> easy convertion to int)
extern int gsYAxisDistanceUM;		// size of Y axis (using µm)
extern int gsSpeed;					// speed in % of max



extern QueueHandle_t quCmdParser;		// queue from Parser to processing module






#endif /* XYPLOTTER_H_ */
