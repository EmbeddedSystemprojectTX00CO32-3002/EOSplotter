/*
 * GParser.h
 *
 *  Created on: 29.09.2017
 *      Author: auril
 */

using namespace std;

#ifndef GPARSER_H_
#define GPARSER_H_


#include "XYPlotter.h"
#include "GParser.h"

#include <string.h>
#include <stdlib.h>		// for atoi
#include "queue.h"
#include "semphr.h"
#include "user_vcom.h"	// for RCV_BUFSIZE from USB to define INPUTLENMAX



#define INPUTLENMAX RCV_BUFSIZE + 1		// max input length for usb commands: USB receive buffer size + '\0'
//#define CMDPARTSMAX 4					// - using CMDSEGMENTSMAX from XYPlotter instead // max parts of a command for the parser (string is exploded to pieces)
#define CMDSEGMENTLENMAX 8 +1			// max len of a part of a command (cmd or argument) +1 for '\0'
#define ENQUEUEMAXWAIT portMAX_DELAY

class GParser {
private:
	// attributes
	char inputstr[INPUTLENMAX];			// last received string from interface
	uint8_t inputstrlen;				// length of inputstr
	uint8_t interfaceselect;			// 0 = USB, 1 = UART (tbd)
	SemaphoreHandle_t muUART;			// only initialized if interfaceselect is UART

	// methods
	int lenstr(char*);					// returns the length of a string (til and including '\0')
	void getRidOfDots(char*);
	// receiving methods
	int receiveString(char*);			// deferrer to selected input function
	int receiveUSB(char*);				// receives a string from USB
	int receiveUART(char*);				// tbd - receives a string from UART (not possible with polling technique? probably it is beceause the Parser Thread is waiting for an input all time (if not parsing currently))
	// sending methods (main sending function is public)
	void sendUSB(uint8_t*, uint32_t);
	void sendUART(char*, int);

public:
	// attributes

	// C&D
	GParser();
	GParser(int chooseInterface);
	virtual ~GParser();

	// methods
	void receiveCommand();
	void sendString(char*, int);

};

#endif /* GPARSER_H_ */
