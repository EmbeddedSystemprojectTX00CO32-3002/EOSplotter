/*
===============================================================================
 Name        : XY Plotter main
 Author      : $(author)
 Version     :
 Copyright   : $(copyright)
 Description : main for xy plotter project
===============================================================================
*/


#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif


#include "XYPlotter.h"
#include "GParser.h"
#include "Command.h"

#include "user_vcom.h"
#include <cr_section_macros.h>
#include "task.h"
#include "ITM_write.h"
#include "semphr.h"				// for semaphores		// freeRTOS
#include "event_groups.h"		// for event groups		// freeRTOS
#include "DigitalIoPin.h"		// for pin input output operations
#include <stdlib.h>				// for atoi
#include <string.h>				// for strcmp

//debug only
#include "DEBUG_CmdQuRcvStub.h"



// DEFINE -----------------------------------------


// GLOBAL -----------------------------------------
EventGroupHandle_t egrp;		// event group 	// for synchronizing stuff - not decided
SemaphoreHandle_t muUART;		// semaphore to access UART (serial port)
QueueHandle_t quCmdParser;		// queue from Parser to processing module

//global settings
int gsXAxisDistanceUM;		// size of Y axis (TODO decision: using mm or mm*100 ("CMM" = centimillimeter) for better usage of commands --> easy convertion to int)
int gsYAxisDistanceUM;		// size of Y axis (TODO decision: using mm or mm*100 CMM
int gsSpeed;				// speed in % of max

DigitalIoPin *switches[3];




//=======================================================================================================
// Standard Functions
//-------------------------------------------------------------------------------------------------------

/* the following is required if runtime statistics are to be collected */
extern "C" {

void vConfigureTimerForRunTimeStats( void ) {
	Chip_SCT_Init(LPC_SCTSMALL1);
	LPC_SCTSMALL1->CONFIG = SCT_CONFIG_32BIT_COUNTER;
	LPC_SCTSMALL1->CTRL_U = SCT_CTRL_PRE_L(255) | SCT_CTRL_CLRCTR_L; // set prescaler to 256 (255 + 1), and start timer
}

}
/* end runtime statictics collection */



/* Set up hardware, interfaces and synchronization mechanics */
static bool SetupHardware(void) {
	SystemCoreClockUpdate();
	Board_Init();

	// synchronization mechanics
	muUART = xSemaphoreCreateMutex();
	quCmdParser = xQueueCreate(QUCMDPARSERLENGTH, sizeof(Command));
	egrp  = xEventGroupCreate();

	//global settings (declarations in XYPlotter.h)
	gsXAxisDistanceUM = 40000;		// size of Y axis (using µm --> easy convertion to int)
	gsYAxisDistanceUM = 40000;		// size of Y axis (using µm)
	gsSpeed = 80;

	// interfacing with hardware - switches and endstops of axes
	switches[0] = new DigitalIoPin(0,17, DigitalIoPin::pullup, true);
	switches[1] = new DigitalIoPin(1,11, DigitalIoPin::pullup, true);
	switches[2] = new DigitalIoPin(1, 9, DigitalIoPin::pullup, true);

	// Debugging
	ITM_init();

	if (egrp == NULL) {
		ITM_write("Event group could not be created!\r\n");
		return false;
	}
	if (muUART == NULL) {
		ITM_write("UART access semaphore could not be created!\r\n");
		return false;
	}
	return true;
}


// ========================================================================================================



/****************************************************************************************************************************
 * Functions ****************************************************************************************************************
 ****************************************************************************************************************************/



/****************************************************************************************************************************
 * Threads ******************************************************************************************************************
 ****************************************************************************************************************************/

//static void task(void* pvParameters) {
//	int tasknr = *(int*) pvParameters;	// convert received void* (parameter int) to int* and dereference it to receive value of the parameter
//}


/******************************************************************************************
 * Task Management ************************************************************************
 ******************************************************************************************/
static void parser_startup_task(void* pvParameter) {
	GParser parser(1);				// create Parser with UART (1) or USB (0)
	vTaskDelay(10);					// wait for the USB CDC task to set up semaphores and stuff
	parser.receiveCommand();		// this method contains the tasks main loop
}


int main(void) {

	if (!SetupHardware()) return 0;		// do not start if hardware was not set up correctly


	// TaskHandles: NULL is important here!! Create Task will return address of TaskHandle and it must be null to do that
	TaskHandle_t parserHandle = NULL;
	TaskHandle_t parserDEBUGHandle = NULL;
//	TaskHandle_t taskHandle3 = NULL;
	static int task1arg = 1;
//	static int task2arg = 2;
//	static int task3arg = 3;

	// USB service task
	xTaskCreate(cdc_task, "CDC",
				configMINIMAL_STACK_SIZE*2, NULL, (tskIDLE_PRIORITY + 1UL),
				(TaskHandle_t *) NULL);

	// GParser task
	xTaskCreate(parser_startup_task, "GParser",	configMINIMAL_STACK_SIZE*6, (void*)&task1arg, (tskIDLE_PRIORITY + 2UL), &parserHandle);
	xTaskCreate(receiveFromParserQueue_DEBUG, "GParserReceiverDEBUG",	configMINIMAL_STACK_SIZE*4, NULL, (tskIDLE_PRIORITY + 2UL), &parserDEBUGHandle);
	// Args:	pvTaskCode, 	 pcName, 		usStackDepth, 			pvParameters, uxPriority, 			pxCreatedTask (byRef)


	/* Start the scheduler */
	vTaskStartScheduler();

	/* Should never arrive here */
	return 1;

}
