/*
 * GParser2.cpp
 *
 *  Created on: 29.09.2017
 *      Author: auril
 */

#include "FreeRTOS.h"
#include "GParser.h"
#include "ITM_write.h"
#include "Command.h"
#include "queue.h"
#include "task.h"
#include "semphr.h"

#define QUCMDPARSERLENGTH 50	// max items on command queue of type Command (Class)
#define CMDSEGMENTSMAX 6		// count of arguments in the arguments array in class Command - TODO should be reduced to the actual max count of arguments of the used commands

extern QueueHandle_t quCmdParser;		// queue from Parser to processing module
extern SemaphoreHandle_t muUART;
extern SemaphoreHandle_t muITM;
//#define INCLUDE_UART		//enable if UART is implemented

GParser::GParser() {
	// TODO Auto-generated constructor stub
	interfaceselect = 0;	// USB = default

}
GParser::GParser(int chooseInterface) {
	quCmdParser = xQueueCreate(QUCMDPARSERLENGTH, sizeof(Command));


	interfaceselect = chooseInterface;
	if (interfaceselect == 1) {
		muUART = xSemaphoreCreateMutex();
		// read some characters in the beginning to remove crap on startup
		xSemaphoreTake(muUART, portMAX_DELAY);

		// JP -> I removed the unused rcvi from this method.
		// Since its just to take away the trash, no need to store it in a var.
		// Will still get returned but thrown into the vortex instead.
		Board_UARTGetChar();
		Board_UARTGetChar();
		Board_UARTGetChar();
		xSemaphoreGive(muUART);
	}
}

GParser::~GParser() {
	// TODO Auto-generated destructor stub
}



//void GParser::receiveFromUSB() {
//
//}

/** returns length of a string excluding the '\0' sign (= index of '\0')
 *
 */


int GParser::lenstr(char* str) {
	int len = 0;
	while(str[len] != '\0') {
		len++;
	}
	return len;
}


/* removes dots in strings (char[])
 * @param: string to be edited (no copy!)
 * @note: function is working until '\0'
 */
void GParser::getRidOfDots(char str[]) {
	int i = 0;
	int o = 0;
	while(str[i] != '\0') {
		if (str[i] != '.') {
			str[o] = str[i];
			i++;
			o++;
		} else {
			i++;
		}

	}
		str[o] = 0;
}


/* This function take a string, searches it for known GCode commands and if regocnized sends them to the queue for processing
 * return values: 0 = not recognized, 1 = ok, 2 = could not send to queue (queue is full, command discarded)
 */
void GParser::receiveCommand() {
	char inputstr[INPUTLENMAX];
	enum statusvalue {ERR = 0, OK = 1, OKsilent = 2, ERR_QueueFull = 102};
	uint8_t status = ERR;

//	char testStr[64]; /// JP -> Added this to debug. Used to show some problem around line 135

	/* status values:
	 * 0 - invalid command
	 * 1 - received correctly
	 * 2 - command received - send no answer back
	 * 102 - Queue is full
	 */
	static unsigned int commandcounter = 0;	//counts invocations of this procedure for debug reasons
	commandcounter++;
	int inputstrlen = 0;

	while(1) {
		char rawcmd[CMDSEGMENTSMAX][CMDSEGMENTLENMAX];	// contains "exploded" version of rawstr (separated at spaces)

		inputstrlen = receiveString(inputstr);

		// EXPLODE
		uint8_t currentpart = 0;			// index of current part to parse into rawcmd
		uint8_t currentindex = 0;			// index of current character inside the currentpart

		// this explodes the string at ' '
		for (int i = 0; i < inputstrlen; i++) {
			if (inputstr[i] == ' ') {
				rawcmd[currentpart][currentindex] = 0;	// terminate current part string
				currentpart++;				// next part
				currentindex = 0;			// reset index for next part
				continue;
			}
			rawcmd[currentpart][currentindex] = inputstr[i];		// put content into currentpart
			currentindex++;
		}
		//terminate last part
		rawcmd[currentpart][currentindex] = 0;
		// /EXPLODE

		uint8_t cmdNr;				// contains number of command (from atoi)


		// JP -> I added this. There is an issue there.

//		snprintf(testStr, 64, "rawcmd00: <%c>\tcmdNr: <%d>\n", rawcmd[0][0], atoi(rawcmd[0]+1));
//		ITM_write(testStr);


		// MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
		if (rawcmd[0][0] == 'M') {
			int value;
			// COMMANDS Mxx
			// find cmd number:
			cmdNr = atoi(rawcmd[0]+1);		// e.g. M10 --> M -> index +1 = "10"; pointer arithmetic
			switch(cmdNr) {
			case 1:	{	// M1
				// SERVO command (M1, code 201)
				value = atoi(rawcmd[1]);
				Command cmdM1(201, value);
				// send to queue ---
				if (xQueueSend(quCmdParser, &cmdM1, ENQUEUEMAXWAIT) == errQUEUE_FULL) {	// direct copy & send to queue, no pointer
					char str[45];
					snprintf(str, 45, "Parser at cmd:%d\targ:%d\tcmdcnt:%d\r\n", cmdM1.getCmd(), cmdM1.getValue(),commandcounter);
					xSemaphoreTake(muITM, portMAX_DELAY);
					ITM_write(str);
					xSemaphoreGive(muITM);

					status = ERR_QueueFull;
				} else {
					status = OK;
				}
				break;
			}
			case 4:	{	// M4
				// LASER command (M4, code 204)
				value = atoi(rawcmd[1]);
				Command cmdM4(204, value);
				// send to queue ---
				if (xQueueSend(quCmdParser, &cmdM4, ENQUEUEMAXWAIT) == errQUEUE_FULL) {	// direct copy & send to queue, no pointer
					char str[45];
					snprintf(str, 45, "Parser at cmd:%d\targ:%d\tcmdcnt:%d\r\n", cmdM4.getCmd(), cmdM4.getValue(),commandcounter);
					xSemaphoreTake(muITM, portMAX_DELAY);
					ITM_write(str);
					xSemaphoreGive(muITM);

					status = ERR_QueueFull;
				} else {
					status = OK;
				}
				break;
			}
			case 10:{	// M10
				// SETTINGS (mDraw asks for settings in firmware)
				// according to Keijo: just send standard string with pseudo settings
				// alternative: send settings from calibration run??, but boardsize must be hardcoded (cannot be measured since stepmode can be changed)
				char strOIU[] = "M10 XY 380 310 0.00 0.00 A0 B0 H0 S80 U160 D90\r\n";
				sendString(strOIU, sizeof(strOIU));

				status = OKsilent;
				break;
			}
			default: 	// Mxx
				// TODO error unknown command
				status = ERR;
			}
		// GGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGGG
		} else if (rawcmd[0][0] == 'G') {
			// COMMANDS Gxx
			// find cmd number:
//			char debugmsg[51];				// TODO remove
			cmdNr = atoi(rawcmd[0]+1);		// e.g. M10 --> M -> index +1 = "10"; pointer arithmetic
			switch(cmdNr) {
			case 1:{	// G1
				// MOVE command (G1, code 101)
				// convert input X105.55 to integer 105550 --> [µm]:
				getRidOfDots(rawcmd[1]);	// eliminate dots in the X argument (destructive modification)
				getRidOfDots(rawcmd[2]);	// eliminate dots in the X argument (destructive modification)
//				for (int c = 'X'; c <='Y'; c++) {
//
//				}
//				if (rawcmd[1] == 'X')
				int xvalue;
				int yvalue;
				int avalue;
				xvalue = atoi(rawcmd[1]+1) * 10;
				yvalue = atoi(rawcmd[2]+1) * 10;
				avalue = atoi(rawcmd[3]+1);
//				snprintf(debugmsg, 51, "cmd to queue: G1: x:<%d> y:<%d> a:<%d>\r\n", xvalue, yvalue, avalue);	// TODO remove
//				ITM_write(debugmsg);
				Command cmdG1(101, xvalue, yvalue, avalue);
				// send to queue ---
				if (xQueueSend(quCmdParser, &cmdG1, ENQUEUEMAXWAIT) == errQUEUE_FULL) {	// direct copy & send to queue, no pointer
					char str[35];
					snprintf(str, 35, "Parser at cmd:%d\targ:%d\tcmdcnt:%d\r\n", cmdG1.getCmd(), cmdG1.getValue(),commandcounter);
					xSemaphoreTake(muITM, portMAX_DELAY);
					ITM_write(str);
					xSemaphoreGive(muITM);

					status = ERR_QueueFull;
				} else {
					status = OK;
				}
				break;
			}
			case 28:{	// G28
				// GO HOME command (G28, code 128)
				Command cmdG28(101,0,0,0);		// move to 0 position
				// send to queue ---
				if (xQueueSend(quCmdParser, &cmdG28, ENQUEUEMAXWAIT) == errQUEUE_FULL) {	// direct copy & send to queue, no pointer
					char str[35];
					snprintf(str, 35, "Parser at G28\tcmdcnt:%d\r\n", commandcounter);
					xSemaphoreTake(muITM, portMAX_DELAY);
					ITM_write(str);
					xSemaphoreGive(muITM);

					status = ERR_QueueFull;
				} else {
					status = OK;
				}
				break;
			}
			default:{	// Gxx
				// TODO Gxx errorhandling
				break;
			}
			}	//end switch
		} else {	// Xxx
			// TODO error unknown command
			status = ERR;
		}

		// ANSWERs & Errorhandling ===============================================================================
		switch(status) {
		case ERR:
			sendString((char*)"ERROR\r\n", 7);		// TODO ITMoutput
			break;
		case OK:
			sendString((char*)"OK\r\n", 4);
			break;
		case OKsilent:
			// send nothing (value is sent inside the case of command recognition)
			break;
		case ERR_QueueFull:
			sendString((char*)"ERROR command queue is full\r\n", 29);
		}

		// TODO: errorhandling with status
	}
}





//====================================================================================================================================
// RECEIVE operations


/** receives a String from selected interface and passes it as reference
 * @param: 	reference to char[] to receive received string
 * @ret: 	length of received string (without '\0')
 * it is assumed that the char[] at the reference is big enought to receive the string!
 */
int GParser::receiveString(char* receivedStr) {
	switch(interfaceselect) {
	case 0:	// USB
		return receiveUSB(receivedStr);
	case 1:	// UART
		return receiveUART(receivedStr);
	default:
		// error
		char str[81];
		snprintf(str, 81, "Error in receiveString: unknown interfaceselect: %d\r\n", interfaceselect);
		xSemaphoreTake(muITM, portMAX_DELAY);
		ITM_write(str);
		xSemaphoreGive(muITM);
	}
	return -1;
}

/** receives a String from USB and passes it as reference
 * @param: 	reference to char[] to receive received string
 * @ret: 	length of received string (without \0)
 * it is assumed that the char[] at the reference is big enought to receive the string!
 */
int GParser::receiveUSB(char* receivedStr) {
	char str[INPUTLENMAX];			// contains currently received string from USB
	char cmdstr[INPUTLENMAX];			// contains accumulated received string
	uint32_t strl = 0;				// length of currently received string
	uint8_t cmdi=0;					// next index for building cmdstr
//	char crlfx[] = "\r\n";


	while (1) {		// receive loop - each USB receive 1 iteration - blocks and waits @ USB_receive call
		// not preserved variables between several (single character) inputs

		// RECEIVING COMMANDS & ARGUMENTS
		strl = USB_receive((uint8_t *)str, INPUTLENMAX-1);
		str[strl] = 0; 								// make sure we have a zero at the end so that we can print the data
		xSemaphoreTake(muITM, portMAX_DELAY);
		ITM_write("\r\nUSB:");
		ITM_write(str);
		xSemaphoreGive(muITM);
		// mirror to console
//		USB_send((uint8_t*) str, strl);

		for (uint8_t stri = 0; stri < strl; stri++) {	// for each letter in a received string (in case more characters are received at once)
			switch (str[stri]) {
			case '\r':		// probably not necessary since mDraw supposedly sends \n at the end of every command
			case '\n':
				cmdstr[cmdi]='\0';					// cmdi always points to the next unused slot
				/* no break */
			case '\0':
//				USB_send((uint8_t*) crlfx, sizeof(crlfx));
				strncpy(receivedStr,cmdstr, cmdi+1);	// length cmdi+1 for \0 sign
				return cmdi;
				//processReceivedCommand(cmdstr, cmdi);		// when \n or \r received, process received command
//				cmdi=0;								// reset for next input
//				cmdstr[0] = 0;						// reset for next input
				break;
			default:
				cmdstr[cmdi] = str[stri];
				cmdi++;
				if (cmdi >= INPUTLENMAX-1) {			// if command is to long for our char array send msg to ITM
					cmdstr[cmdi]='\0';

					char debugmsg[41];
					snprintf(debugmsg, 41, "\r\nCommand is too long: %s\r\n", cmdstr);
					xSemaphoreTake(muITM, portMAX_DELAY);
					ITM_write(debugmsg);
					xSemaphoreGive(muITM);
					receivedStr = cmdstr;
					return cmdi;
//					cmdi=0;							// reset for next input
//					cmdstr[0] = 0;					// reset for next input
				}
				break;
			}
		}
	}
}


int GParser::receiveUART(char* receivedStr) {
//	char str[INPUTLENMAX];		// contains currently received string from USB
//	char cmdstr[INPUTLENMAX];	// contains accumulated received string
//	uint32_t strl = 0;			// length of currently received string
//	uint8_t cmdi;				// next index for building cmdstr
//	char rcvc;					// for DEBUG reasons only TODO delete it
	int rcvi;					// current received letter from UART
	int cnt = 0;				// counter for symbols


	while (1) {
		xSemaphoreTake(muUART, portMAX_DELAY);
		rcvi = Board_UARTGetChar();
		xSemaphoreGive(muUART);

//		rcvc = rcvi;			// for debug TODO delete line
		if (rcvi == EOF) {	// whenever nothing is received EOF is returned! sometimes 255 is returned?! <<<<<<<<<<<<<<<<<< wtf?
			// nothing	// if EOF was received
			vTaskDelay(1);	// 1ms delay
		} else {			// sth has been received --> analyze!
			if (rcvi == '\n') {
				receivedStr[cnt++] = rcvi;	// add \n
				receivedStr[cnt] = 0;		// terminate
				return cnt;					// return complete string
			} else {
				receivedStr[cnt] = rcvi;			// accumulate string
//				Board_UARTPutChar(rcvi);
				cnt++;
			}
		}
	}
	return -1; // code never goes to here
}



//====================================================================================================================================
// SEND operations

/** send operations
 * for sending answers, flexible for different hardware interfaces
 *
 */
void GParser::sendString(char* str, int len) {
	switch(interfaceselect) {
	case 0:	// USB
		sendUSB((uint8_t*) str, len);
		break;
	case 1:	// UART
		sendUART(str, len);
		break;
	default:
		// error
		char str[81];
		snprintf(str, 81, "Error in sendString: unknown interfaceselect: %d\r\n", interfaceselect);
		xSemaphoreTake(muITM, portMAX_DELAY);
		ITM_write(str);
		xSemaphoreGive(muITM);

	}
}

void GParser::sendUSB(uint8_t* data, uint32_t len) {
	USB_send(data, len);
}

void GParser::sendUART(char* str, int len) {
	// use mutex (not yet implemented)
	xSemaphoreTake(muUART, portMAX_DELAY);
	Board_UARTPutSTR(str);
	xSemaphoreGive(muUART);
}


