///*
// * USBinput.cpp
// *
// *  Created on: 04.10.2017
// *      Author: auril
// */
//
//#include "USBinput.h"
//
//USBinput::USBinput() {
//	// TODO Auto-generated constructor stub
//
//}
//
//USBinput::~USBinput() {
//	// TODO Auto-generated destructor stub
//}
//
//
//
///** receives a String from USB and passes it as reference
// * @param: 	reference to char[] to receive received string
// * @ret: 	length of received string (without \0)
// * it is assumed that the char[] at the reference is big enought to receive the string!
// */
//static int USBinput::receiveString(char* receivedStr) {
//	char str[INPUTLENMAX];			// contains currently received string from USB
//	char cmdstr[INPUTLENMAX];			// contains accumulated received string
//	uint32_t strl = 0;				// length of currently received string
//	uint8_t cmdi;					// next index for building cmdstr
//	char crlfx[] = "\r\n";
//
//
//	while (1) {		// receive loop - each USB receive 1 iteration - blocks and waits @ USB_receive call
//		// not preserved variables between several (single character) inputs
//
//		// RECEIVING COMMANDS & ARGUMENTS
//		strl = USB_receive((uint8_t *)str, INPUTLENMAX-1);
//		str[strl] = 0; 								// make sure we have a zero at the end so that we can print the data
//		ITM_write(str);
//		// mirror to console
//		USB_send((uint8_t*) str, strl);
//
//		for (uint8_t stri = 0; stri < strl; stri++) {	// for each letter in a received string (in case more characters are received at once)
//			switch (str[stri]) {
//			case '\r':
//			case '\n':
//				cmdstr[cmdi]='\0';					// qi always points to the next unused slot
//			case '\0':
//				USB_send((uint8_t*) crlfx, sizeof(crlfx));
//				receivedStr = cmdstr;
//				return cmdi;
//				//processReceivedCommand(cmdstr, cmdi);		// when \n or \r received, process received command
////				cmdi=0;								// reset for next input
////				cmdstr[0] = 0;						// reset for next input
//				break;
//			default:
//				cmdstr[cmdi] = str[stri];
//				cmdi++;
//				if (cmdi >= INPUTLENMAX-1) {			// if command is to long for our char array send msg to ITM
//					cmdstr[cmdi]='\0';
//
//					char debugmsg[41];
//					sprintf(debugmsg, "\r\nCommand is too long: %s\r\n", cmdstr);
//					ITM_write(debugmsg);
//
//					receivedStr = cmdstr;
//					return cmdi;
////					cmdi=0;							// reset for next input
////					cmdstr[0] = 0;					// reset for next input
//				}
//				break;
//			}
//		}
//	}
//}
