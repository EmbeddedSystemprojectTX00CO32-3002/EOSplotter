/*
 * Command.cpp
 *
 *  Created on: 29.09.2017
 *      Author: auril
 */

#include "Command.h"


// auto empty constructor
Command::Command() {
	cmd = -1;	// invalid
}
// constructor for move commands
Command::Command(int cmd, int X, int Y, int A) {
	Command::cmd = cmd;
	Command::x = X;
	Command::y = Y;
	Command::a = A;
}

//constructor for value commands
Command::Command(int cmd, int value) {
	Command::cmd = cmd;
	Command::value = value;
}

Command::~Command() {
	// TODO Auto-generated destructor stub
}

// GETTER methods
int Command::getCmd() {
	return cmd;
}
int Command::getX() {
	return x;
}
int Command::getY() {
	return y;
}
int Command::getA() {
	return a;
}
int Command::getValue() {
	return value;
}

// there's nothing to set for SETTER
