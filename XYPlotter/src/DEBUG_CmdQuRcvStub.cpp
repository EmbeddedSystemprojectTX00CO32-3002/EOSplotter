#include "FreeRTOS.h"
#include "queue.h"
#include "Command.h"
#include <map>
#include "ITM_write.h"
#include <cstring>
#include "semphr.h"
#include "task.h"
using namespace std;

extern QueueHandle_t quCmdParser;
extern SemaphoreHandle_t muITM;

void receiveFromParserQueue_DEBUG(void* pvParam) {
	Command cmd;
	char cmdmsg[100];
	char gcode[4];
//	std::map<int, char[4]> cmdcodes;
//	cmdcodes[101] = "G1";
//	cmdcodes[128] = "G28";
//	cmdcodes[201] = "M1";
//	cmdcodes[204] = "M4";
//	cmdcodes[210] = "M10";




	while(1) {
		xQueueReceive(quCmdParser, &cmd, portMAX_DELAY);

		switch(cmd.getCmd()) {
			case 101:
				strcpy(gcode, "G1");
				break;
			case 128:
				strcpy(gcode, "G28");
				break;
			case 201:
				strcpy(gcode, "M1");
				break;
			case 204:
				strcpy(gcode, "M4");
				break;
			case 210:
				strcpy(gcode, "M10");
				break;
		}

		snprintf(cmdmsg, 100, "Command:%d (%s)\tX:%d\tY:%d\tA:%d\tV:%d\r\n", cmd.getCmd(), gcode, cmd.getX(), cmd.getY(), cmd.getA(), cmd.getValue());
		xSemaphoreTake(muITM, portMAX_DELAY);
		ITM_write(cmdmsg);
		xSemaphoreGive(muITM);

		vTaskDelay(1);	// ITM needs some time to print and prints garbage if output happens too fast.


	}
}
